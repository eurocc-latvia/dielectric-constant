#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<omp.h>

#include"PoleZero.h"


#ifdef MP
	#include<mpi.h>
#endif

//------------------------------------------------------------------------------------------------------------------
// VARIABLE INTIALIZATION
//------------------------------------------------------------------------------------------------------------------

const int NUM_IT[] = {44001, 4001};

const char path[100] 		= "./poles_zeros";
const char pathpoly[100] 	= "./poly_coeff";

//------------------------------------------------------------------------------------------------------------------

int main(int argc,char * argv[]){

	int numprocs=1.0, rank=0.0, namelen, provided;
	cdouble RT[2];
	int sign;

#ifdef MP

	char procname[MPI_MAX_PROCESSOR_NAME];
	MPI_Status status;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
	MPI_Comm_rank(MPI_COMM_WORLD, &rank); 
	MPI_Get_processor_name(procname, &namelen);

#endif


//--------------------------------------------------------------------------------------

int chunk, chunk0, add_size;

double coeff;
double zri[2];

#ifdef MP
	if(numprocs > 49){
		chunk	 = 0;
		chunk0	 = 1;
		add_size = 49;
	}else if(numprocs > 1){
		chunk 	 = static_cast<int>(floor(49.0/(1.0*numprocs)));
		chunk0 	 = ++chunk;
		add_size = 49 % numprocs;
	}else{
		chunk  	 = 0;  
		chunk0 	 = 49;
		add_size = 1;	
	}
#else 
	chunk  	 = 0;  
//	chunk0 	 = 49;
	chunk0 	 = 1;
	add_size = 1; 
	rank = 0;
#endif


//----------------------------------------------------------------------------

for(int m=0; m<2; ++m){

	if(rank >= 49){
		break;
	}

	sign = (m==0) ? -1 : 1;


	for(int id=0; id<((rank < add_size) ? chunk0 : chunk); ++id){

		if(rank < add_size){
			al = 0.51 + (chunk0*rank + id)*dal;
		}else{
			al = 0.51 + (chunk0*add_size + chunk*(rank-add_size) + id)*dal;
		}

		NUM0 = 440;

		coeff = NUM0*dr;

		POLES_OPEN(al)
		POLES_READ_R(al)
		POLES_CLOSE

		for(int st=0; st < NUM_IT[m]; ++st){

			r = coeff + 0.001 + sign*st*dr*0.01;

			kor = 2.0*PI*al*r;

			//-----------------------------------------------------------------------------

			besselj(0, kor, BJo, 2*M+1);
			bessely(0, kor, BYo, 2*M+1);

			besscyl_freq(al, PQPt, M, IP);

			for(int n=0;n<NPOLE;++n){	
				zri[0] = creal(lpo[n]);
				zri[1] = cimag(lpo[n]);
				nelder_mead(funmin_pos, zri, 0.01, 0.0000001, 400);
				lpo[n] = zri[0] + I*zri[1];
//				printf("poles = %5.12f,%5.12f\n", lpo[n]);
			}
			for(int n=0;n<NPOLE;++n){	
				zri[0] = creal(lno[n]);
				zri[1] = cimag(lno[n]);
				nelder_mead(funmin_neg, zri, 0.01, 0.0000001, 400);
				lno[n] = zri[0] + I*zri[1];
			}
			if(m == 0){
				if(st % 100 == 0 && st > 0){
					--NUM0; 
					POLES_OPENW(al)
					POLES_WRITE_P(al)
					POLES_WRITE_N(al)
					POLES_CLOSE
				}
			}else{
				if(st % 100 == 0 && st > 0){
					++NUM0; 				
					POLES_OPENW(al)
					POLES_WRITE_P(al)
					POLES_WRITE_N(al)
					POLES_CLOSE
				}
			}

//			printf("NUM0=%d\n", NUM0);

		}

	}

}

#ifdef MP
	MPI_Finalize();
#endif

	return 0;

}

//------------------------------------------------------------------------------

double funmin_pos(double * z){

	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];

	if(z[0] > -2.0){
		return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'p')));
	}else{
     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
		return -cabs(RT[0]+RT[1]);
	}

}	

double funmin_neg(double * z){

	cdouble RT[2];
	cdouble zp = z[0]+I*z[1];

	if(z[0] > -2.0){
		return -1.0/(cabs(cylindr(z[0]+I*z[1], r, al, BJPt, PQPt, M, 'n')));
	}else{
     		cylindrRT(z[0]+I*z[1], r, al, BJPt, PQPt, RT, M);
		return -cabs(RT[0]-RT[1]);
	}
}	




