#!/bin/bash

MPATH=/mnt/home/romanetf/GIT_HPC/dielectricconstantmeasuncertainty/MonteCarloEst

cd $MPATH

rm -f ./PoleZero

mpic++  PoleZero.cpp -o PoleZero -L./lib -lrod -lewald -lslatec -llapack -lrefblas -lgfortran  -DMP

mpirun ./PoleZero

cd ~
