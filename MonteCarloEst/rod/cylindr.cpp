#include<stdio.h>
#include<complex.h>
#include<math.h>
#include<omp.h>

#define cdouble _Complex double 


#define P1(i,j) PQ[0][i-1+(j-1)*M] 
#define P2(i,j) PQ[1][i-1+(j-1)*M] 
#define Q1(i,j) PQ[2][i-1+(j-1)*M] 
#define Q2(i,j) PQ[3][i-1+(j-1)*M] 

#define M1(i,j) M1[i-1+(j-1)*M] 
#define M2(i,j) M2[i-1+(j-1)*M] 

#define gam1(i)  gam[2*i-2] 
#define gamp1(i) gamp[2*i-2] 

#define gam2(i)  gam[2*i-1] 
#define gamp2(i) gamp[2*i-1] 

#define X1(i) X1[i-1]
#define X2(i) X2[i-1]

#define pivot(i) pivot[i-1]


#define PI 3.141592653589793116


void besselj(int,cdouble,cdouble *,int);
void bessely(int,cdouble,cdouble *,int);

void besscyl_rad(cdouble eps, double r, double al, cdouble * BJ[],cdouble * gampt[], const int M);

extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);
extern "C" void zgetrf_(int*,int*,cdouble*,int*,int*,int*);


cdouble cylindr(cdouble eps, double r,double al, cdouble * BJPt[],cdouble * PQ[], int M,const char mode)
{

cdouble kor = 2.0*PI*al*r;

int m,n,id,itmp,counter;
cdouble deter = 1.0;

int info, pivot[M]; 

cdouble M1[M*M], M2[M*M];
cdouble X1[M], X2[M];

cdouble   gam[2*M], gamp[2*M];
cdouble * gamPt[] = {gam,gamp};

besscyl_rad(eps, r, al, BJPt, gamPt, M);

for(n=1; n<=M; ++n){
   for(m=1; m<=M; ++m){
          M1(m,n) = gamp1(m)*P1(m,n) + gam1(m)*Q1(m,n);
          M2(m,n) = gamp2(m)*P2(m,n) + gam2(m)*Q2(m,n);       
       if(n==1){        
          X1(m) = gamp1(m)*(creal(P1(m,n))-I*cimag(P1(m,n))) + gam1(m)*(creal(Q1(m,n))-I*cimag(Q1(m,n)));
          X2(m) = gamp2(m)*(creal(P2(m,n))-I*cimag(P2(m,n))) + gam2(m)*(creal(Q2(m,n))-I*cimag(Q2(m,n)));
       } 
   }
}

if(mode=='p'){
  
   zgetrf_(&M,&M,M1,&M,pivot,&info);
  
   for(id=1;id<=M;++id){
      deter *= M1(id,id);     
   }
}
else{
    
    zgetrf_(&M,&M,M2,&M,pivot,&info);
  
    for(id=1;id<=M;++id){
      deter *= M2(id,id);       
   }
}

counter = 0; 

for(m=1;m<=(M-1);++m){
   for(n=1;n<=(M-n);++n){   
      if(pivot(m) > pivot(m+1)){

        itmp = pivot(m);
        pivot(m) = pivot(m+1);
        pivot(m+1) = itmp;          
       
        ++counter;
       
        if(counter==2){
           counter-=2;   
        }
      }   
   }
}

if(counter==0){
return deter;
}
else{
return -1.0*deter;
}


}
