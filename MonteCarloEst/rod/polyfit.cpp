#include<stdio.h>
#include<complex.h>

#define cdouble _Complex double


#define A(i,j)  A[(i-1)+(j-1)*N]

#define B1(i,j) B1[(i-1)+(j-1)*ORD]
#define B2(i,j) B2[(i-1)+(j-1)*ORD]

#define ra1(i)  ra1[i-1]
#define ra2(i)  ra2[i-1]

#define rb(i)   rbpt[i-1]
#define rb1(i)  rbpt[0][i-1]
#define rb2(i)  rbpt[1][i-1]

#define l1(i)   lpt[0][i-1]
#define l2(i)   lpt[1][i-1]


extern "C" void zgesv_(int*,int*,cdouble*,int*,int*,cdouble*,int*,int*);

void cylindrRT(cdouble,double,double,cdouble *BJPt[],cdouble *PQ[],cdouble * RT,int);

cdouble fapp(cdouble,cdouble *,const int);


void polyfit(double epsmin,double epsmax,double epsimg,double r,double al, cdouble * BJPt[],cdouble * PQPt[],cdouble *lpt[], cdouble *rbpt[], const int M, const int N,const int NE, int ORD){


int m,n,k;
int nrhs = 1;
int info, pivot[ORD];


cdouble A[N*ORD], B1[ORD*ORD], B2[ORD*ORD], ra1[N], ra2[N];
cdouble RT[2];

cdouble deps = (epsmax-epsmin)/((double) N);
cdouble epsinit = epsmin + I*epsimg;
cdouble eps;


for(m=1;m<=N;++m){
   eps = epsinit;
   cylindrRT(eps,r,al,BJPt,PQPt,RT,M);
   epsinit += deps;
// ra1(m) = carg((RT[0] + RT[1])/fapp(eps,lpt[0],NE));
// ra2(m) = carg((RT[0] - RT[1])/fapp(eps,lpt[1],NE));
   ra1(m) = ((RT[0] + RT[1])/fapp(eps,lpt[0],NE));
   ra2(m) = ((RT[0] - RT[1])/fapp(eps,lpt[1],NE));

// printf("ra1 = %5.14f,%5.14f\n", ra2(m));

}

epsinit = epsmin + I*epsimg;

for(k=1;k<=N;++k){
    for(m=1;m<=ORD;++m){
        if(m==1){   
           A(k,ORD+1-m) = 1.0;    
        }else{
           A(k,ORD+1-m) = A(k,ORD+2-m)*epsinit; 
        }
    }
    epsinit += deps;
}

for(m=1;m<=ORD;++m){
   for(n=1;n<=ORD;++n){       
      for(k=1;k<=N;++k){
          if(k==1){
             B1(m,n) = conj(A(k,ORD+1-m))*A(k,n); 
          }else if(k==N){
             B1(m,n)+= conj(A(k,ORD+1-m))*A(k,n); 
             B2(m,n) = B1(m,n);
          }else{
             B1(m,n)+= conj(A(k,ORD+1-m))*A(k,n); 
          }
      }
   }
}

for(m=1;m<=ORD;++m){
   for(k=1;k<=N;++k){
      if(k==1){
         rb1(m) = conj(A(k,ORD+1-m))*ra1(k); 
         rb2(m) = conj(A(k,ORD+1-m))*ra2(k); 
      }else{
         rb1(m) += conj(A(k,ORD+1-m))*ra1(k); 
         rb2(m) += conj(A(k,ORD+1-m))*ra2(k); 
      }
   }
}


zgesv_(&ORD,&nrhs,B1,&ORD,pivot,rb(1),&ORD,&info);
zgesv_(&ORD,&nrhs,B2,&ORD,pivot,rb(2),&ORD,&info);

/*
for(m=1;m<=ORD;++m){
    printf("rb1 = %5.14E,%5.14E\n", rb1(m));
}
*/

} 
